<?php
/**
 *Plugin Name: Culture Cube Gform SharpSpring
 *Description: Integrating gravity forms on CC website with SharpSpring
 *Author: Greg Lorenzen
 *Version: 1.0.6
**/
    // post_to_third_party_3 = Journey With Us Form (1)

    add_action( 'gform_after_submission_3', 'post_to_third_party_3', 10, 2 );

    function post_to_third_party_3( $entry, $form ) {
        $baseURI = 'https://app-3QND11VFRQ.marketingautomation.services/webforms/receivePostback/MzawMDEzNbSwBAA/';
        $endpoint = '1f93e636-a41b-467a-8390-e30b2a2220b3';
        $post_url = $baseURI . $endpoint;

        $body = array(
            'First Name' => rgar( $entry, '3.3' ),
            'Last Name' => rgar( $entry, '3.6' ),
            'Preferred Email' => rgar( $entry, '7' ),
            'Company Name' => rgar( $entry, '9' ),
            'Phone' => rgar( $entry, '5' ),
            'How Can We Help' => rgar( $entry, '10' ),
            'trackingid__sb' => $_COOKIE['__ss_tk']
        );
        $request = new WP_Http();

        $response = $request->post( $post_url, array( 'body' => $body ) );
    }
?>